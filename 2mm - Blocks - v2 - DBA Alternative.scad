// 0.015
//G_SCALE = 1;
G_SCALE = 1/60;
G_SCALE_VEC = [G_SCALE,G_SCALE,G_SCALE];

G_PERSON_AVERAGE_HEIGHT = 180;
G_HORSE_AVERAGE_HEIGHT = 160;

include <Lib\regular_shapes.scad>;

module person(mount = "none")
{
    module addHorse(horseSize,sLegs,sTorso)
    {
        colors = ["Brown","Sienna","SaddleBrown","Brown","Sienna","SaddleBrown","Brown","Sienna","SaddleBrown","LemonChiffon","Gainsboro","LightGray","Silver","Ivory","Beige"];
            
        color(colors[round(rands(0,len(colors)-1,1)[0])])
        scale(G_SCALE_VEC)
        {
        translate([-horseSize[0]/2+sLegs[0]/2-sTorso[0]/2,-2*sTorso[1]/2,-0.1])
            {
                difference()
                {
                    cube(horseSize);
                    translate([0.5*horseSize[0],1.5*horseSize[1],0])
                    {
                        rotate([90,0,0])
                        triangle_prism(1.3*horseSize[2],0.66*horseSize[2]);
                    }
                }
                
                horseNeck = [horseSize[1]*0.8,horseSize[1],horseSize[2]*1.1];
                horseHead = [horseNeck[0]*1.5,horseSize[1],horseSize[1]/2];
                translate([horseSize[0]-horseNeck[0],0,horseSize[2]/2])
                {
                    rotate([0,15,0])
                    cube(horseNeck);
                    translate([horseNeck[0]-horseHead[0]/4,0,horseNeck[2]-2*horseHead[2]])
                    rotate([0,-10,0])
                    cube(horseHead);
                }
            }
        }
    }
    
    module addCamel(camelSize,sLegs,sTorso)
    {
        colors = ["Sienna"];
        color(colors[round(rands(0,len(colors)-1,1)[0])])
        scale(G_SCALE_VEC)
        {
        translate([-camelSize[0]/2+sLegs[0]/2-sTorso[0]/2,-2*sTorso[1]/2,-0.1])
            {
                difference()
                {
                    cube(camelSize);
                    translate([0.5*camelSize[0],1.5*camelSize[1],0])
                    {
                        rotate([90,0,0])
                        triangle_prism(1.3*camelSize[2],0.66*camelSize[2]);
                    }
                }
                
                camelNeck = [camelSize[1]*0.8,camelSize[1],camelSize[2]*0.8];
                camelHead = [camelSize[0]*0.5,camelSize[1],camelSize[1]/2];
                translate([camelSize[0]-camelNeck[0],0,camelSize[2]/2+10])
                {
                    rotate([0,35,0])
                    {
                        cube(camelNeck);
                        translate([7,0,camelNeck[2]-30])
                        {
                            rotate([0,-25,0])
                            scale([1,1,0.4])
                            cube(camelNeck);
                            translate([-21,0,48])
                            {
                                rotate([0,-35,0])
                                {
                                    scale([1,1,0.4])
                                        cube(camelNeck);
                                    cube(camelHead);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    module addElephant(mountSize)
    {
        scale(G_SCALE_VEC)
        {
            // Body
            color("Grey")
            translate([-50,0,200])
            rotate([0,-10,0])
            scale([2,1,1])
            sphere(d = mountSize[1],$fn=60);
            
            // Snout
            color("Grey")
            translate([mountSize[1]*1.2,0,mountSize[1]/2*1.5])
            rotate([0,5,0])
            scale([0.3,0.3,1.5])
            sphere(d = mountSize[1]*0.6,$fn=60);
            
            // Ear
            color("Grey")
            translate([mountSize[1],0,mountSize[1]*1.1])
            rotate([0,-5,0])
            scale([1,0.8,1.3])
            sphere(d = mountSize[1]*0.6,$fn=60);
            
            // Ear
            color("Grey")
            translate([mountSize[1],-mountSize[1]/4,mountSize[1]*1.2])
            rotate([0,0,-35])
            scale([0.3,0.6,1])
            sphere(d = mountSize[1]*0.6,$fn=60);
            
            // Right front leg
            color("Grey")
            translate([mountSize[1],mountSize[1]/4,mountSize[1]*1.2])
            rotate([0,0,35])
            scale([0.3,0.6,1])
            sphere(d = mountSize[1]*0.6,$fn=60);
            
            // Left front leg
            color("Grey")
            translate([mountSize[1]*0.6,-0.5*mountSize[1]/2,130])
            scale([0.7,0.7,2])
            sphere(d = mountSize[1]/2);
            
            color("Grey")
            translate([mountSize[1]*0.6,0.5*mountSize[1]/2,130])
            scale([0.7,0.7,2])
            sphere(d = mountSize[1]/2);
            
            color("Grey")
            translate([-mountSize[1]*0.8,-0.5*mountSize[1]/2,110])
            scale([0.7,0.7,2])
            sphere(d = mountSize[1]/2);
            
            color("Grey")
            translate([-mountSize[1]*0.8,0.5*mountSize[1]/2,110])
            scale([0.7,0.7,2])
            sphere(d = mountSize[1]/2);
        }
    }

    PersonHeight = rands(165,190,1)[0];
    PersonHead = PersonHeight / 10;
    PersonLegHeight = PersonHead * 4.5;
    PersonTorsoHeight = PersonHead * 3.5;
    PersonWidth = PersonHead * 2;

    horseSize = [240,2*PersonWidth,rands(140,180,1)[0]];
    camelSize = [240,2*PersonWidth,rands(195,215,1)[0]];
    elephantSize = [rands(550,750,1)[0],rands(250,340,1)[0],350];
    
    function getMountSize() = mount == "horse" ? horseSize : isCamel(mount);
    function isCamel(mount) = mount == "camel" ? camelSize : isElephant(mount);
    function isElephant(mount) = mount == "elephant" ? elephantSize : isChariot(mount);
    function isChariot(mount) = mount == "chariot" ? chariotSize : [0,0,0];

    mountSize = getMountSize();
    
    if(mount == "horse")
        addHorse(mountSize,[PersonWidth,PersonWidth,PersonWidth],[PersonWidth,PersonWidth,PersonWidth]);
    if(mount == "camel")
        addCamel(mountSize,[PersonWidth,PersonWidth,PersonWidth],[PersonWidth,PersonWidth,PersonWidth]);
    if(mount == "elephant")
        addElephant(mountSize);

    skinColors = ["NavajoWhite","Wheat","Bisque","Burlywood","Tan"];
    skinColor = skinColors[round(rands(0,len(skinColors)-1,1)[0])];
    helmetColors = ["Goldenrod","Goldenrod","DarkGoldenrod","DarkGoldenrod","Gainsboro","Silver","Gray","DarkGray"];
    helmetColor = helmetColors[round(rands(0,len(helmetColors)-1,1)[0])];
    armorColors = ["Goldenrod","Goldenrod","DarkGoldenrod","DarkGoldenrod","Gainsboro","Silver","Gray","DarkGray", "Cornsilk", "RoyalBlue", "LightBlue", "Yellow", "Khaki", "Crimson"];
    armorColor = armorColors[round(rands(0,len(armorColors)-1,1)[0])];

    scale(G_SCALE_VEC)
    {
        translate([0,0,max(mountSize[2]-0.7*PersonLegHeight,0)])
        {
            color(skinColor) cylinder(PersonLegHeight,PersonWidth*0.6,PersonWidth*0.8);
            translate([0,0,PersonLegHeight])
            {
                color(armorColor)cylinder(PersonTorsoHeight,PersonWidth,PersonWidth);
                translate([0,0,PersonTorsoHeight])
                {
                    color(skinColor)cylinder(2*PersonHead,PersonHead,PersonHead);
                }
            }
        }
    }
}

module spear(SpearHeightMin = 200, SpearHeightMax = -1, z= 0)
{
    spearColors = ["Peru","Chocolate","SaddleBrown","Sienna"];
    spearColor = spearColors[rands(0,len(spearColors)-1,1)[0]];
    
    SpearHeight = SpearHeightMax != -1 ? rands(SpearHeightMin,SpearHeightMax,1)[0] : 
                                            SpearHeightMin;
    scale(G_SCALE_VEC)
    {
        translate([-15,-60,z])
            color(spearColor)cube([30,30,SpearHeight]);
    }
}

module javelin(SpearHeightMin = 150, SpearHeightMax = -1)
{
    spearColors = ["Peru","Chocolate","SaddleBrown","Sienna"];
    spearColor = spearColors[rands(0,len(spearColors)-1,1)[0]];
    
    SpearHeight = SpearHeightMax != -1 ? rands(SpearHeightMin,SpearHeightMax,1)[0] : 
                                            SpearHeightMin;
    scale(G_SCALE_VEC)
    {
        translate([-SpearHeight/2,-35,150])
		rotate([rands(5,25,1)[0],90,0])
            color(spearColor)cube([30,30,SpearHeight]);
    }
}

module roundShield(ShieldDiameter = 100,oval=false, z=80)
{
    shieldColors = ["SaddleBrown","Sienna","Gray","Silver","Gainsboro","Red","RoyalBlue","Ivory","Beige","Tan","LightYellow"];
    shieldColor = shieldColors[round(rands(0,len(shieldColors)-1,1)[0])];
    
    scale(G_SCALE_VEC)
    {
        translate([0,20,z])
        rotate([0,90,rands(5,35,1)[0]])
        {
            if(!oval)
                color(shieldColor)
                cylinder(50,ShieldDiameter/2,ShieldDiameter/2,$fn=60);
            else
            {
                scale([1,0.7,1])
                color(shieldColor)
                cylinder(50,ShieldDiameter/2,ShieldDiameter/2,$fn=60);
            }
        }
    }
}

module addLine(num,separation,scatter=[1,1],type)
{
    offsetY = separation[1]/2;
    offsetX = separation[0]/2;
    translate([0,G_SCALE*offsetY,0])
    for(pos=[0:separation[1]*G_SCALE:num*(separation[1]*G_SCALE)-(separation[1]*G_SCALE)])
    {
        scatterAmount = [
        rands(-(separation[0]/2)*scatter[0],(separation[0]/2)*scatter[0],1)[0],
        rands(-separation[1]/2*scatter[1],separation[1]/2*scatter[1],1)[0]];
        translate([G_SCALE*(offsetX+scatterAmount[0]),(pos+G_SCALE*scatterAmount[1]),0])
        {
            
            if(type=="Spear")
            {
                person();
                spear(200,250);
                roundShield(60);
            }
            if(type=="Hoplite")
            {
                person();
                spear(250,270);
                roundShield(100);
            }
            if(type=="Phalanx")
            {
                person();
                spear(500,550);
                roundShield(60);
            }
            if(type=="Light infantry")
            {
                person();
                roundShield(60);
            }
            if(type=="Medium infantry")
            {
                person();
                roundShield(80);
            }
            if(type=="Heavy infantry")
            {
                person();
                roundShield(110,true);
            }
			if(type=="Javelin")
			{
                person();
				javelin();
				roundShield(60);
			}
            if(type=="Bow")
            {
                person();
            }
            if(type=="Cavalry")
            {
                person("horse");
                spear(z = G_HORSE_AVERAGE_HEIGHT)
                roundShield(60,false,50+G_HORSE_AVERAGE_HEIGHT);
            }
            if(type=="Camelry")
            {
                person("camel");
                spear(z = G_HORSE_AVERAGE_HEIGHT)
                roundShield(60,false,50+G_HORSE_AVERAGE_HEIGHT);
            }
            if(type=="Knight")
            {
                person("horse");
                spear(380,400, z = G_HORSE_AVERAGE_HEIGHT);
                roundShield(80, z = 50+G_HORSE_AVERAGE_HEIGHT);
            }
            if(type=="Elephant")
            {
                translate([0,0,0])
                person("elephant");
            }
        }
    }
}

module addUnit(num,lines,separation,scatter=[0,0],type="none")
{
    for(pos=[0:separation[0]*G_SCALE:lines*(separation[0]*G_SCALE)-(separation[0]*G_SCALE)])
    {
        translate([pos,0,0])
        {
            addLine(num,separation,scatter,type);
        }
    }
}

module addBase(line,numLines,separation,scatter=[0,0],unitType,height = 1/G_SCALE)
{
    width=line*(separation[1]*G_SCALE);
    length=numLines*(separation[0]*G_SCALE);
	color("SaddleBrown")
    cube([length,width,height*G_SCALE]);
    translate([0,0,height*G_SCALE])
        addUnit(line,numLines,separation,scatter,unitType);
}

module addEmptyBase(length=2*150,width=16*150,height = 1/G_SCALE)
{
	color("SaddleBrown")
    cube([length*G_SCALE,width*G_SCALE,height*G_SCALE]);
}

module addTroop(numInLine, linesInBlock, blocks, separations, scatter, type)
{
    lineLength = linesInBlock * separations[0]*G_SCALE;
    lineSeparation = separations[2];
    
    for(pos=[0:1:blocks-1])
    {
        translate([pos*(lineLength+lineSeparation*G_SCALE),0,0])
        {
            if(pos<blocks-1)
            {
                addBase(numInLine,linesInBlock,separations,scatter,type);
                translate([lineLength,0,0])
                    addEmptyBase(lineSeparation,numInLine*separations[1]);
            }
            else
                addBase(numInLine,linesInBlock,separations,scatter,type);
        }
    }
}

module 6bd()
{
    numInLine = 9;
    linesInBlock = 2;
    blocks = 6;
    separations = [150,120,120];
    scatter = [0,0];
    type = "Heavy infantry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    translate([0,numInLine*separations[1]*G_SCALE,0])
    {
        addEmptyBase(linesInBlock*blocks*separations[0]+(blocks-1)*separations[2],2*separations[1]);
        translate([0,2*separations[1]*G_SCALE,0])
            addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    }
}

module 4bd()
{
    numInLine = 9;
    linesInBlock = 4;
    blocks = 1;
    separations = [150,120,200];
    scatter = [0,0];
    type = "Heavy infantry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    translate([0,numInLine*separations[1]*G_SCALE,0])
    {
        addEmptyBase(linesInBlock*blocks*separations[0]+(blocks-1)*separations[2],2*separations[1]);
        translate([0,2*separations[1]*G_SCALE,0])
            addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    }
}

module 3bd()
{
    numInLine = 9;
    linesInBlock = 3;
    blocks = 1;
    separations = [201,120,150];
    scatter = [0,0];
    type = "Heavy infantry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    translate([0,numInLine*separations[1]*G_SCALE,0])
    {
        addEmptyBase(linesInBlock*blocks*separations[0]+(blocks-1)*separations[2],2*separations[1]);
        translate([0,2*separations[1]*G_SCALE,0])
            addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    }
}

module 4ax()
{
    numInLine = 7;
    linesInBlock = 4;
    blocks = 1;
    separations = [300,150,200];
    scatter = [0.4,0.4];
    type = "Spear";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    translate([0,numInLine*separations[1]*G_SCALE,0])
    {
        addEmptyBase(linesInBlock*blocks*separations[0]+(blocks-1)*separations[2],2*separations[1]);
        translate([0,2*separations[1]*G_SCALE,0])
            addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    }
}

module 3ax()
{
    numInLine = 7;
    linesInBlock = 3;
    blocks = 1;
    separations = [401,150,150];
    scatter = [0.4,0.4];
    type = "Spear";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    translate([0,numInLine*separations[1]*G_SCALE,0])
    {
        addEmptyBase(linesInBlock*blocks*separations[0]+(blocks-1)*separations[2],2*separations[1]);
        translate([0,2*separations[1]*G_SCALE,0])
            addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    }
}

module sp()
{
	numInLine = 16;
    linesInBlock = 4;
    blocks = 1;
    separations = [225,150,200];
    scatter = [0,0];
    type = "Spear";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
}

module 8sp()
{
	numInLine = 16;
    linesInBlock = 8;
    blocks = 1;
    separations = [225,150,150];
    scatter = [0,0];
    type = "Spear";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
}

module 4pk()
{
    numInLine = 20;
    linesInBlock = 4;
    blocks = 1;
    separations = [225,120,150];
    scatter = [0,0];
    type = "Phalanx";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
}

module 3pk()
{
    numInLine = 20;
    linesInBlock = 3;
    blocks = 1;
    separations = [401,120,150];
    scatter = [0,0];
    type = "Phalanx";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
}

module 4wb()
{
    numInLine = 8;
    linesInBlock = 4;
    blocks = 1;
    separations = [120,300,100];
    scatter = [0.3,0.5];
    type = "Medium infantry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
}

module 3wb()
{
    numInLine = 8;
    linesInBlock = 3;
    blocks = 1;
    separations = [150,300,150];
    scatter = [0.3,0.5];
    type = "Medium infantry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
}

module 7hd()
{
    numInLine = 20;
    linesInBlock = 2;
    blocks = 7;
    separations = [110,120,180];
    scatter = [0.3,0.4];
    type = "Light infantry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module 5hd()
{
    numInLine = 20;
    linesInBlock = 3;
    blocks = 5;
    separations = [110,120,180];
    scatter = [0.3,0.4];
    type = "Light infantry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module ps()
{
    numInLine = 8;
    linesInBlock = 4;
    blocks = 1;
    separations = [300,300,200];
    scatter = [0.6,0.6];
    type = "Javelin";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module 8bw()
{
    numInLine = 12;
    linesInBlock = 8;
    blocks = 1;
    separations = [300,200,175];
    scatter = [0.2,0.4];
    type = "Bow";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module 4bw()
{
    numInLine = 12;
    linesInBlock = 4;
    blocks = 1;
    separations = [300,200,200];
    scatter = [0.2,0.4];
    type = "Bow";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module 3bw()
{
    numInLine = 12;
    linesInBlock = 3;
    blocks = 1;
    separations = [401,200,380];
    scatter = [0.2,0.4];
    type = "Bow";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module cv()
{
    numInLine = 8;
    linesInBlock = 4;
    blocks = 1;
    separations = [450,300,200];
    scatter = [0.2,0.2];
    type = "Cavalry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module 6cv()
{
    numInLine = 8;
    linesInBlock = 6;
    blocks = 1;
    separations = [600,300,300];
    scatter = [0.3,0.4];
    type = "Cavalry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module 6kn()
{
    numInLine = 12;
    linesInBlock = 6;
    blocks = 1;
    separations = [600,200,200];
    scatter = [0.2,0.2];
    type = "Knight";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module 4kn()
{
    numInLine = 12;
    linesInBlock = 4;
    blocks = 1;
    separations = [450,200,200];
    scatter = [0.2,0.2];
    type = "Knight";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module 3kn()
{
    numInLine = 5;
    linesInBlock = 3;
    blocks = 1;
    separations = [600,200,200];
    scatter = [0.2,0.2];
    type = "Knight";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    translate([0,numInLine*separations[1]*G_SCALE,0])
    {
        addEmptyBase(linesInBlock*blocks*separations[0]+(blocks-1)*separations[2],2*separations[1]);
        translate([0,2*separations[1]*G_SCALE,0])
            addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    }
}

module cm()
{
    numInLine = 8;
    linesInBlock = 3;
    blocks = 1;
    separations = [600,300,200];
    scatter = [0.2,0.4];
    type = "Camelry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module el()
{
    numInLine = 4;
    linesInBlock = 3;
    blocks = 1;
    separations = [900,600,200];
    scatter = [0.2,0.7];
    type = "Elephant";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	 
}

3ax();
