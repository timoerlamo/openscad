// 0.015
//G_SCALE = 1;
G_SCALE = 1/50;
G_SCALE_VEC = [G_SCALE,G_SCALE,G_SCALE];

G_PERSON_AVERAGE_HEIGHT = 180;
G_HORSE_AVERAGE_HEIGHT = 160;

include <..\Lib\regular_shapes.scad>;

module person(mount = "none")
{
    module addHorse(horseSize,sLegs,sTorso)
    {
        colors = ["Brown","Sienna","SaddleBrown","Brown","Sienna","SaddleBrown","Brown","Sienna","SaddleBrown","LemonChiffon","Gainsboro","LightGray","Silver","Ivory","Beige"];
            
        color(colors[round(rands(0,len(colors)-1,1)[0])])
        scale(G_SCALE_VEC)
        {
        translate([-horseSize[0]/2+sLegs[0]/2-sTorso[0]/2,-2*sTorso[1]/2,-0.1])
            {
                difference()
                {
                    cube(horseSize);
                    translate([0.5*horseSize[0],1.5*horseSize[1],0])
                    {
                        rotate([90,0,0])
                        triangle_prism(1.3*horseSize[2],0.66*horseSize[2]);
                    }
                }
                
                horseNeck = [horseSize[1]*0.8,horseSize[1],horseSize[2]*1.1];
                horseHead = [horseNeck[0]*1.5,horseSize[1],horseSize[1]/2];
                translate([horseSize[0]-horseNeck[0],0,horseSize[2]/2])
                {
                    rotate([0,15,0])
                    cube(horseNeck);
                    translate([horseNeck[0]-horseHead[0]/4,0,horseNeck[2]-2*horseHead[2]])
                    rotate([0,-10,0])
                    cube(horseHead);
                }
            }
        }
    }
    
    module addCamel(camelSize,sLegs,sTorso)
    {
        colors = ["Sienna"];
        color(colors[round(rands(0,len(colors)-1,1)[0])])
        scale(G_SCALE_VEC)
        {
        translate([-camelSize[0]/2+sLegs[0]/2-sTorso[0]/2,-2*sTorso[1]/2,-0.1])
            {
                difference()
                {
                    cube(camelSize);
                    translate([0.5*camelSize[0],1.5*camelSize[1],0])
                    {
                        rotate([90,0,0])
                        triangle_prism(1.3*camelSize[2],0.66*camelSize[2]);
                    }
                }
                
                camelNeck = [camelSize[1]*0.8,camelSize[1],camelSize[2]*0.8];
                camelHead = [camelSize[0]*0.5,camelSize[1],camelSize[1]/2];
                translate([camelSize[0]-camelNeck[0],0,camelSize[2]/2+10])
                {
                    rotate([0,35,0])
                    {
                        cube(camelNeck);
                        translate([7,0,camelNeck[2]-30])
                        {
                            rotate([0,-25,0])
                            scale([1,1,0.4])
                            cube(camelNeck);
                            translate([-21,0,48])
                            {
                                rotate([0,-35,0])
                                {
                                    scale([1,1,0.4])
                                        cube(camelNeck);
                                    cube(camelHead);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    module addElephant(mountSize)
    {
        scale(G_SCALE_VEC)
        {
            // Body
            color("Grey")
            translate([-50,0,200])
            rotate([0,-10,0])
            scale([2,1,1])
            sphere(d = mountSize[1],$fn=60);
            
            // Snout
            color("Grey")
            translate([mountSize[1]*1.2,0,mountSize[1]/2*1.5])
            rotate([0,5,0])
            scale([0.3,0.3,1.5])
            sphere(d = mountSize[1]*0.6,$fn=60);
            
            // Ear
            color("Grey")
            translate([mountSize[1],0,mountSize[1]*1.1])
            rotate([0,-5,0])
            scale([1,0.8,1.3])
            sphere(d = mountSize[1]*0.6,$fn=60);
            
            // Ear
            color("Grey")
            translate([mountSize[1],-mountSize[1]/4,mountSize[1]*1.2])
            rotate([0,0,-35])
            scale([0.3,0.6,1])
            sphere(d = mountSize[1]*0.6,$fn=60);
            
            // Right front leg
            color("Grey")
            translate([mountSize[1],mountSize[1]/4,mountSize[1]*1.2])
            rotate([0,0,35])
            scale([0.3,0.6,1])
            sphere(d = mountSize[1]*0.6,$fn=60);
            
            // Left front leg
            color("Grey")
            translate([mountSize[1]*0.6,-0.5*mountSize[1]/2,130])
            scale([0.7,0.7,2])
            sphere(d = mountSize[1]/2);
            
            color("Grey")
            translate([mountSize[1]*0.6,0.5*mountSize[1]/2,130])
            scale([0.7,0.7,2])
            sphere(d = mountSize[1]/2);
            
            color("Grey")
            translate([-mountSize[1]*0.8,-0.5*mountSize[1]/2,110])
            scale([0.7,0.7,2])
            sphere(d = mountSize[1]/2);
            
            color("Grey")
            translate([-mountSize[1]*0.8,0.5*mountSize[1]/2,110])
            scale([0.7,0.7,2])
            sphere(d = mountSize[1]/2);
        }
    }

    PersonHeight = rands(165,190,1)[0];
    PersonHead = PersonHeight / 10;
    PersonLegHeight = PersonHead * 4.5;
    PersonTorsoHeight = PersonHead * 3.5;
    PersonWidth = PersonHead * 2;

    horseSize = [240,2*PersonWidth,rands(140,180,1)[0]];
    camelSize = [240,2*PersonWidth,rands(195,215,1)[0]];
    elephantSize = [rands(550,750,1)[0],rands(250,340,1)[0],350];
    
    function getMountSize() = mount == "horse" ? horseSize : isCamel(mount);
    function isCamel(mount) = mount == "camel" ? camelSize : isElephant(mount);
    function isElephant(mount) = mount == "elephant" ? elephantSize : isChariot(mount);
    function isChariot(mount) = mount == "chariot" ? chariotSize : [0,0,0];

    mountSize = getMountSize();
    
    if(mount == "horse")
        addHorse(mountSize,[PersonWidth,PersonWidth,PersonWidth],[PersonWidth,PersonWidth,PersonWidth]);
    if(mount == "camel")
        addCamel(mountSize,[PersonWidth,PersonWidth,PersonWidth],[PersonWidth,PersonWidth,PersonWidth]);
    if(mount == "elephant")
        addElephant(mountSize);

    skinColors = ["NavajoWhite","Wheat","Bisque","Burlywood","Tan"];
    skinColor = skinColors[round(rands(0,len(skinColors)-1,1)[0])];
    helmetColors = ["Goldenrod","Goldenrod","DarkGoldenrod","DarkGoldenrod","Gainsboro","Silver","Gray","DarkGray"];
    helmetColor = helmetColors[round(rands(0,len(helmetColors)-1,1)[0])];
    armorColors = ["Goldenrod","Goldenrod","DarkGoldenrod","DarkGoldenrod","Gainsboro","Silver","Gray","DarkGray", "Cornsilk", "RoyalBlue", "LightBlue", "Yellow", "Khaki", "Crimson"];
    armorColor = armorColors[round(rands(0,len(armorColors)-1,1)[0])];

    scale(G_SCALE_VEC)
    {
        translate([0,0,max(mountSize[2]-0.7*PersonLegHeight,0)])
        {
            color(skinColor) cylinder(PersonLegHeight,PersonWidth,PersonWidth);
            translate([0,0,PersonLegHeight])
            {
                color(armorColor)cylinder(PersonTorsoHeight,1.3*PersonWidth,1.3*PersonWidth);
                translate([0,0,PersonTorsoHeight])
                {
                    color(skinColor)cylinder(3*PersonHead,2*PersonHead,2*PersonHead);
                }
            }
        }
    }
}

module spear(SpearHeightMin = 200, SpearHeightMax = -1, z= 0)
{
    spearColors = ["Peru","Chocolate","SaddleBrown","Sienna"];
    spearColor = spearColors[rands(0,len(spearColors)-1,1)[0]];
    
    SpearHeight = SpearHeightMax != -1 ? rands(SpearHeightMin,SpearHeightMax,1)[0] : 
                                            SpearHeightMin;
    scale(G_SCALE_VEC)
    {
        translate([-15,-60,z])
            color(spearColor)cube([30,30,SpearHeight]);
    }
}

module javelin(SpearHeightMin = 150, SpearHeightMax = -1)
{
    spearColors = ["Peru","Chocolate","SaddleBrown","Sienna"];
    spearColor = spearColors[rands(0,len(spearColors)-1,1)[0]];
    
    SpearHeight = SpearHeightMax != -1 ? rands(SpearHeightMin,SpearHeightMax,1)[0] : 
                                            SpearHeightMin;
    scale(G_SCALE_VEC)
    {
        translate([-SpearHeight/2,-35,150])
		rotate([rands(5,25,1)[0],90,0])
            color(spearColor)cube([30,30,SpearHeight]);
    }
}

module roundShield(ShieldDiameter = 100,oval=false, z=100)
{
    shieldColors = ["SaddleBrown","Sienna","Gray","Silver","Gainsboro","Red","RoyalBlue","Ivory","Beige","Tan","LightYellow"];
    shieldColor = shieldColors[round(rands(0,len(shieldColors)-1,1)[0])];
    
    scale(G_SCALE_VEC)
    {
        translate([0,20,z])
        rotate([0,90,rands(5,35,1)[0]])
        {
            if(!oval)
                color(shieldColor)
                cylinder(50,ShieldDiameter/2,ShieldDiameter/2,$fn=60);
            else
            {
                scale([1,0.7,1])
                color(shieldColor)
                cylinder(50,ShieldDiameter/2,ShieldDiameter/2,$fn=60);
            }
        }
    }
}

module addLine(num,separation,scatter=[1,1],type)
{
    offsetY = separation[1]/2;
    offsetX = separation[0]/2;
    translate([0,G_SCALE*offsetY,0])
    for(pos=[0:separation[1]*G_SCALE:num*(separation[1]*G_SCALE)-(separation[1]*G_SCALE)])
    {
        scatterAmount = [
        rands(-(separation[0]/2)*scatter[0],(separation[0]/2)*scatter[0],1)[0],
        rands(-separation[1]/2*scatter[1],separation[1]/2*scatter[1],1)[0]];
        translate([G_SCALE*(offsetX+scatterAmount[0]),(pos+G_SCALE*scatterAmount[1]),0])
        {
            
            if(type=="Spear")
            {
                person();
                spear(250,300);
                roundShield(60);
            }
            if(type=="Hoplite")
            {
                person();
                spear(300,450);
                roundShield(100);
            }
            if(type=="Phalanx")
            {
                person();
                spear(500,550);
                roundShield(60);
            }
            if(type=="Light infantry")
            {
                person();
                spear(200,250);
                roundShield(60);
            }
            if(type=="Medium infantry")
            {
                person();
                roundShield(80);
            }
            if(type=="Heavy infantry")
            {
                person();
                roundShield(110,true);
            }
            if(type=="Slinger")
			{
                person();
				roundShield(60);
			}
			if(type=="Javelin")
			{
                person();
				javelin();
				roundShield(60);
			}
            if(type=="Bow")
            {
                person();
            }
            if(type=="Cavalry")
            {
                person("horse");
                spear(z = G_HORSE_AVERAGE_HEIGHT)
                roundShield(60,false,50+G_HORSE_AVERAGE_HEIGHT);
            }
            if(type=="Camelry")
            {
                person("camel");
                spear(z = G_HORSE_AVERAGE_HEIGHT)
                roundShield(60,false,50+G_HORSE_AVERAGE_HEIGHT);
            }
            if(type=="Knight")
            {
                person("horse");
                spear(380,400, z = G_HORSE_AVERAGE_HEIGHT);
                roundShield(80, z = 50+G_HORSE_AVERAGE_HEIGHT);
            }
            if(type=="Elephant")
            {
                translate([0,0,0])
                person("elephant");
            }
        }
    }
}

module addUnit(num,lines,separation,scatter=[0,0],type="none")
{
    for(pos=[0:separation[0]*G_SCALE:lines*(separation[0]*G_SCALE)-(separation[0]*G_SCALE)])
    {
        translate([pos,0,0])
        {
            addLine(num,separation,scatter,type);
        }
    }
}

module addBase(line,numLines,separation,scatter=[0,0],unitType,height = 1/G_SCALE, heightMultiplier = 2)
{
    height = height * heightMultiplier;
    width=line*(separation[1]*G_SCALE);
    length=numLines*(separation[0]*G_SCALE);
	color("SaddleBrown")
    cube([length,width,height*G_SCALE]);
    translate([0,0,height*G_SCALE])
        addUnit(line,numLines,separation,scatter,unitType);
}

module addEmptyBase(length=2*150,width=16*150,height = 1/G_SCALE, heightMultiplier = 2)
{
    height = height * heightMultiplier;
	color("SaddleBrown")
    cube([length*G_SCALE,width*G_SCALE,height*G_SCALE]);
}

module addTroop(numInLine, linesInBlock, blocks, separations, scatter, type)
{
    lineLength = linesInBlock * separations[0]*G_SCALE;
    lineSeparation = separations[2];
    
    for(pos=[0:1:blocks-1])
    {
        translate([pos*(lineLength+lineSeparation*G_SCALE),0,0])
        {
            if(pos<blocks-1)
            {
                addBase(numInLine,linesInBlock,separations,scatter,type);
                translate([lineLength,0,0])
                    addEmptyBase(lineSeparation,numInLine*separations[1]);
            }
            else
                addBase(numInLine,linesInBlock,separations,scatter,type);
        }
    }
}

module 6bd(widthMultiplier = 1)
{
    numInLine = 9*widthMultiplier;
    linesInBlock = 2;
    blocks = 6;
    separations = [150,120,120];
    scatter = [0,0];
    type = "Heavy infantry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    translate([0,numInLine*separations[1]*G_SCALE,0])
    {
        addEmptyBase(linesInBlock*blocks*separations[0]+(blocks-1)*separations[2],2*separations[1]);
        translate([0,2*separations[1]*G_SCALE,0])
            addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    }
}

module 4bd(widthMultiplier = 1)
{
    numInLine = 9*widthMultiplier;
    linesInBlock = 2;
    blocks = 4;
    separations = [120,120,100];
    scatter = [0,0];
    type = "Heavy infantry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
}

module 4ax(widthMultiplier = 1)
{
    numInLine = 7*widthMultiplier;
    linesInBlock = 2;
    blocks = 4;
    separations = [150,150,200];
    scatter = [0.4,0.4];
    type = "Spear";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    translate([0,numInLine*separations[1]*G_SCALE,0])
    {
        addEmptyBase(linesInBlock*blocks*separations[0]+(blocks-1)*separations[2],2*separations[1]);
        translate([0,2*separations[1]*G_SCALE,0])
            addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    }
}

module LightInfantry(widthMultiplier = 1)
{
    numInLine = 7*widthMultiplier;
    linesInBlock = 2;
    blocks = 3;
    separations = [150,150,150];
    scatter = [0.4,0.4];
    type = "Light infantry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);

}

module MediumInfantry(widthMultiplier = 1)
{
    numInLine = 8*widthMultiplier;
    linesInBlock = 2;
    blocks = 3;
    separations = [120,125,150];
    scatter = [0,0];
    type = "Heavy infantry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
}

module HeavyInfantry(widthMultiplier = 1)
{
	numInLine = 8*widthMultiplier;
    linesInBlock = 2;
    blocks = 3;
    separations = [120,125,150];
    scatter = [0,0];
    type = "Hoplite";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
}

module Auxilia(widthMultiplier = 1)
{
	numInLine = 8*widthMultiplier;
    linesInBlock = 2;
    blocks = 3;
    separations = [120,125,150];
    scatter = [0,0];
    type = "Spear";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
}

module 8sp(widthMultiplier = 1)
{
	numInLine = 16*widthMultiplier;
    linesInBlock = 2;
    blocks = 8;
    separations = [120,150,150];
    scatter = [0,0];
    type = "Spear";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
}

module 4pk(widthMultiplier = 1)
{
    numInLine = 20*widthMultiplier;
    linesInBlock = 2;
    blocks = 4;
    separations = [120,120,150];
    scatter = [0,0];
    type = "Phalanx";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
}

module 3pk(widthMultiplier = 1)
{
    numInLine = 20*widthMultiplier;
    linesInBlock = 3;
    blocks = 3;
    separations = [120,120,150];
    scatter = [0,0];
    type = "Phalanx";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
}

module 4wb(widthMultiplier = 1)
{
    numInLine = 8*widthMultiplier;
    linesInBlock = 2;
    blocks = 4;
    separations = [120,300,100];
    scatter = [0.3,0.5];
    type = "Medium infantry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
}

module Warrior(widthMultiplier = 1)
{
    numInLine = 4*widthMultiplier;
    linesInBlock = 7;
    blocks = 1;
    separations = [150,300,150];
    scatter = [0.7,0.7];
    type = "Medium infantry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
}

module 7hd(widthMultiplier = 1)
{
    numInLine = 20*widthMultiplier;
    linesInBlock = 2;
    blocks = 7;
    separations = [110,120,180];
    scatter = [0.3,0.4];
    type = "Light infantry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module 5hd(widthMultiplier = 1)
{
    numInLine = 20*widthMultiplier;
    linesInBlock = 3;
    blocks = 5;
    separations = [110,120,180];
    scatter = [0.3,0.4];
    type = "Light infantry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module Slinger(widthMultiplier = 1)
{
    numInLine = 4*widthMultiplier;
    linesInBlock = 4;
    blocks = 1;
    separations = [300,300,200];
    scatter = [0.8,0.6];
    type = "Slinger";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module Javelin(widthMultiplier = 1)
{
    numInLine = 4*widthMultiplier;
    linesInBlock = 4;
    blocks = 1;
    separations = [300,300,200];
    scatter = [0.8,0.6];
    type = "Javelin";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module 8bw(widthMultiplier = 1)
{
    numInLine = 12*widthMultiplier;
    linesInBlock = 8;
    blocks = 1;
    separations = [300,200,175];
    scatter = [0.2,0.4];
    type = "Bow";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module 4bw(widthMultiplier = 1)
{
    numInLine = 12*widthMultiplier;
    linesInBlock = 4;
    blocks = 1;
    separations = [300,200,200];
    scatter = [0.2,0.4];
    type = "Bow";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module LightBow(widthMultiplier = 1)
{
    numInLine = 6*widthMultiplier;
    linesInBlock = 3;
    blocks = 1;
    separations = [400,200,380];
    scatter = [0.2,0.4];
    type = "Bow";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module LightCavalry(widthMultiplier = 1)
{
    numInLine = 3*widthMultiplier;
    linesInBlock = 2;
    blocks = 1;
    separations = [500,335,200];
    scatter = [0.2,0.2];
    type = "Cavalry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module MediumCavalry(widthMultiplier = 1)
{
    numInLine = 4*widthMultiplier;
    linesInBlock = 2;
    blocks = 1;
    separations = [500,250,200];
    scatter = [0.2,0.2];
    type = "Cavalry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module HeavyCavalry(widthMultiplier = 1)
{
    numInLine = 7*widthMultiplier;
    linesInBlock = 3;
    blocks = 1;
    separations = [400,150,200];
    scatter = [0.2,0.2];
    type = "Knight";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module 6cv(widthMultiplier = 1)
{
    numInLine = 8*widthMultiplier;
    linesInBlock = 6;
    blocks = 1;
    separations = [600,300,300];
    scatter = [0.3,0.4];
    type = "Cavalry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module 6kn(widthMultiplier = 1)
{
    numInLine = 12*widthMultiplier;
    linesInBlock = 6;
    blocks = 1;
    separations = [600,200,200];
    scatter = [0.2,0.2];
    type = "Knight";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module 3kn(widthMultiplier = 1)
{
    numInLine = 5*widthMultiplier;
    linesInBlock = 3;
    blocks = 1;
    separations = [450,200,200];
    scatter = [0.2,0.2];
    type = "Knight";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    translate([0,numInLine*separations[1]*G_SCALE,0])
    {
        addEmptyBase(linesInBlock*blocks*separations[0]+(blocks-1)*separations[2],2*separations[1]);
        translate([0,2*separations[1]*G_SCALE,0])
            addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);
    }
}

module cm(widthMultiplier = 1)
{
    numInLine = 8*widthMultiplier;
    linesInBlock = 3;
    blocks = 1;
    separations = [600,300,200];
    scatter = [0.2,0.4];
    type = "Camelry";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	
}

module el(widthMultiplier = 1)
{
    numInLine = 4*widthMultiplier;
    linesInBlock = 3;
    blocks = 1;
    separations = [900,600,200];
    scatter = [0.2,0.7];
    type = "Elephant";
    
    addTroop(numInLine,linesInBlock,blocks,separations,scatter,type);	 
}


module test(wm = 1)
{
    shiftY = 3000*wm;
    shiftX = 3000;
    translate([0,0*shiftY*G_SCALE,0]) 6bd(wm);
    translate([0,1*shiftY*G_SCALE,0]) 4bd(wm);
    translate([0,2*shiftY*G_SCALE,0]) 3bd(wm);
    translate([0,3*shiftY*G_SCALE,0]) 4ax(wm);
    translate([0,4*shiftY*G_SCALE,0]) 3ax(wm);
    translate([0,5*shiftY*G_SCALE,0]) sp(wm);
    translate([0,6*shiftY*G_SCALE,0]) 8sp(wm);

    translate([2*shiftX*G_SCALE,0*shiftY*G_SCALE,0]) 4pk(wm);
    translate([2*shiftX*G_SCALE,1*shiftY*G_SCALE,0]) 3pk(wm);
    translate([2*shiftX*G_SCALE,2*shiftY*G_SCALE,0]) 4wb(wm);
    translate([2*shiftX*G_SCALE,3*shiftY*G_SCALE,0]) 3wb(wm);
    translate([2*shiftX*G_SCALE,4*shiftY*G_SCALE,0]) 7hd(wm);
    translate([2*shiftX*G_SCALE,5*shiftY*G_SCALE,0]) 5hd(wm);

    translate([4*shiftX*G_SCALE,0*shiftY*G_SCALE,0]) ps(wm);
    translate([4*shiftX*G_SCALE,1*shiftY*G_SCALE,0]) 8bw(wm);
    translate([4*shiftX*G_SCALE,2*shiftY*G_SCALE,0]) 4bw(wm);
    translate([4*shiftX*G_SCALE,3*shiftY*G_SCALE,0]) 3bw(wm);

    translate([6*shiftX*G_SCALE,0*shiftY*G_SCALE,0]) 6cv(wm);
    translate([6*shiftX*G_SCALE,1*shiftY*G_SCALE,0]) cv(wm);
    translate([6*shiftX*G_SCALE,2*shiftY*G_SCALE,0]) 6kn(wm);
    translate([6*shiftX*G_SCALE,3*shiftY*G_SCALE,0]) 4kn(wm);
    translate([6*shiftX*G_SCALE,4*shiftY*G_SCALE,0]) 3kn(wm);

    translate([8*shiftX*G_SCALE,0*shiftY*G_SCALE,0]) cm(wm);
    translate([8*shiftX*G_SCALE,1*shiftY*G_SCALE,0]) el(wm);
}

HeavyCavalry();